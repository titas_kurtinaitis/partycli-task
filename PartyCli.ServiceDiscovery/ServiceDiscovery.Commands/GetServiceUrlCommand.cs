﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using ServiceDiscovery.DataContracts.Dto;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses.Commands;
using ServiceDiscovery.Repository;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;

namespace ServiceDiscovery.Commands
{
    public class GetServiceUrlCommand : ICommand<GetServiceUrlRequest, CommandResponse<GetServiceUrlResponse>>
    {
        private readonly IServiceRepository serviceRepository;
        private readonly ILogger<GetServiceUrlCommand> logger;

        public GetServiceUrlCommand(IServiceRepository serviceRepository, ILogger<GetServiceUrlCommand> logger)
        {
            this.logger = logger;
            this.serviceRepository = serviceRepository;
        }

        public CommandResponse<GetServiceUrlResponse> Execute(GetServiceUrlRequest request)
        {
            this.logger.LogDebug("Started executing GetServiceUrl command.");
            var serviceResponse = serviceRepository.GetServicesConfiguration();
            serviceResponse.InternalServices = serviceResponse.InternalServices ?? new List<ServiceConfigurationDto>();
            var foundService = serviceResponse.InternalServices.FirstOrDefault(x =>
                string.Compare(x.Name, request.ServiceName, StringComparison.OrdinalIgnoreCase) == 0);

            if (foundService != null)
            {
                this.logger.LogDebug($"Service: {request.ServiceName} was found and host url is: {foundService.HostUrl}.");

                return new CommandResponse<GetServiceUrlResponse>
                {
                    Result = new GetServiceUrlResponse
                    {
                        ServiceUrl = foundService.HostUrl
                    },
                    ResponseType = CommandResponseType.Success
                };
            }

            this.logger.LogError($"Service: {request.ServiceName} was not found.");

            return new CommandResponse<GetServiceUrlResponse>
            {
                Error = new ErrorResult
                {
                    ErrorMessage = "Service was not found."
                },
                ResponseType = CommandResponseType.NotFound
            };
        }
    }
}