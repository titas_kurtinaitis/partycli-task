﻿using System.Collections.Generic;
using ServiceDiscovery.DataContracts.Dto;

namespace ServiceDiscovery.DataContracts.Configurations
{
    public class ServicesConfiguration
    {
        public List<ServiceConfigurationDto> InternalServices { get; set; }
    }
}
