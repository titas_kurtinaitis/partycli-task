﻿namespace ServiceDiscovery.DataContracts.Requests
{
    public class GetServiceUrlRequest
    {
        public string ServiceName { get; set; }
    }
}
