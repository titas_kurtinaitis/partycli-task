﻿namespace ServiceDiscovery.DataContracts.Dto
{
    public class ServiceConfigurationDto
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string HostUrl { get; set; }
    }
}
