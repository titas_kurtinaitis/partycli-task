﻿using System.Collections.Generic;
using ServiceDiscovery.DataContracts.Dto;

namespace ServiceDiscovery.DataContracts.Responses.Repositories
{
    public class GetServicesConfigurationResponse
    {
        public GetServicesConfigurationResponse()
        {
            this.InternalServices = new List<ServiceConfigurationDto>();
        }

        public List<ServiceConfigurationDto> InternalServices { get; set; }
    }
}
