﻿namespace ServiceDiscovery.DataContracts.Responses.Commands
{
    public class GetServiceUrlResponse
    {
        public string ServiceUrl { get; set; }
    }
}
