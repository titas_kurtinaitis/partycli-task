﻿using ServiceDiscovery.DataContracts.Responses.Repositories;

namespace ServiceDiscovery.Repository
{
    public interface IServiceRepository
    {
        GetServicesConfigurationResponse GetServicesConfiguration();
    }
}