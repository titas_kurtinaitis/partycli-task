﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ServiceDiscovery.DataContracts.Configurations;
using ServiceDiscovery.DataContracts.Dto;
using ServiceDiscovery.DataContracts.Responses.Repositories;

namespace ServiceDiscovery.Repository
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly ServicesConfiguration servicesConfiguration;
        private readonly ILogger<ServiceRepository> logger;

        public ServiceRepository(IOptions<ServicesConfiguration> serviceConfigurationOptions, ILogger<ServiceRepository> logger)
        {
            this.logger = logger;
            this.servicesConfiguration = serviceConfigurationOptions.Value ?? throw new ArgumentNullException(nameof(serviceConfigurationOptions), "Service Configurations are not defined for Service Discovery.");
        }

        public GetServicesConfigurationResponse GetServicesConfiguration()
        {
            this.logger.LogDebug("Getting services list.");
            var response = new GetServicesConfigurationResponse
            {
                InternalServices = servicesConfiguration.InternalServices ?? new List<ServiceConfigurationDto>()
            };

            return response;
        }
    }
}
