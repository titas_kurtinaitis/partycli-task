﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceDiscovery.Commands;
using ServiceDiscovery.DataContracts.Configurations;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses.Commands;
using ServiceDiscovery.Repository;
using SharedComponents.CommandsHandler;

namespace ServiceDiscovery
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var builder = new ContainerBuilder();

            services.Configure<ServicesConfiguration>(Configuration.GetSection(nameof(ServicesConfiguration)));

            // Register Commands
            builder.RegisterType<GetServiceUrlCommand>().As<ICommand<GetServiceUrlRequest, CommandResponse<GetServiceUrlResponse>>>();

            // Register Repositories
            builder.RegisterType<ServiceRepository>().As<IServiceRepository>().SingleInstance();

            builder.Populate(services);
            this.ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
