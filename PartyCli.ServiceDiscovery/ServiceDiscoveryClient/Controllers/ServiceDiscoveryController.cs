﻿using Microsoft.AspNetCore.Mvc;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses.Commands;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;
using SharedComponents.ResponseHandler;

namespace ServiceDiscovery.Controllers
{
    [Route("service-discovery")]
    public class ServiceDiscoveryController : ApiController
    {
        private readonly ICommand<GetServiceUrlRequest, CommandResponse<GetServiceUrlResponse>> getServiceUrlCommand;

        public ServiceDiscoveryController(ICommand<GetServiceUrlRequest, CommandResponse<GetServiceUrlResponse>> getServiceUrlCommand)
        {
            this.getServiceUrlCommand = getServiceUrlCommand;
        }

        [HttpGet("{name}")]
        public IActionResult GetServiceUrl(string name)
        {
            var commandRequest = new GetServiceUrlRequest
            {
                ServiceName = name
            };

            var commandResponse = this.getServiceUrlCommand.Execute(commandRequest);

            if (commandResponse.ResponseType != CommandResponseType.Success)
            {
                return this.CommandErrorResponse(commandResponse);
            }

            return this.Ok(commandResponse.Result);
        }
    }
}
