﻿using Microsoft.AspNetCore.Mvc;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;
using SharedComponents.ResponseHandler;
using UserService.DataContracts.Requests;
using UserService.DataContracts.Requests.Commands;
using UserService.DataContracts.Responses.Commands;

namespace UserService.Controllers
{
    [Route("users")]
    public class UsersController : ApiController
    {
        private readonly ICommand<CreateUserCommandRequest, CommandResponse<CreateUserCommandResponse>> createUserCommand;
        private readonly ICommand<GetLatestUserCommandRequest, CommandResponse<GetLatesetUserCommandResponse>> getLatestUserCommand;

        public UsersController(ICommand<CreateUserCommandRequest, CommandResponse<CreateUserCommandResponse>> createUserCommand, ICommand<GetLatestUserCommandRequest, CommandResponse<GetLatesetUserCommandResponse>> getLatestUserCommand)
        {
            this.getLatestUserCommand = getLatestUserCommand;
            this.createUserCommand = createUserCommand;
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] CreateUserRequest createUserRequest)
        {
            var commandRequest = new CreateUserCommandRequest
            {
                Password = createUserRequest.Password,
                Username = createUserRequest.Username
            };

            var commandResponse = this.createUserCommand.Execute(commandRequest);

            if (commandResponse.ResponseType != CommandResponseType.Success)
            {
                return this.CommandErrorResponse(commandResponse);
            }

            return this.Ok(commandResponse.Result);
        }

        [HttpGet("queries/get-latest-user")]
        public IActionResult GetLatestUser()
        {
            var commandResponse = this.getLatestUserCommand.Execute(new GetLatestUserCommandRequest());

            if (commandResponse.ResponseType != CommandResponseType.Success)
            {
                return this.CommandErrorResponse(commandResponse);
            }

            return this.Ok(commandResponse.Result);
        }
    }
}
