﻿using System;
using System.Collections.Generic;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharedComponents.CommandsHandler;
using UserService.Commands;
using UserService.DataContracts.Requests.Commands;
using UserService.DataContracts.Responses.Commands;
using UserService.Repository;

namespace UserService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var builder = new ContainerBuilder();

            // Register Commands
            builder.RegisterType<CreateUserCommand>().As<ICommand<CreateUserCommandRequest, CommandResponse<CreateUserCommandResponse>>>();
            builder.RegisterType<GetLatestUserCommand>().As<ICommand<GetLatestUserCommandRequest, CommandResponse<GetLatesetUserCommandResponse>>>();

            // Register Repositories
            builder.RegisterType<UsersReadRepository>().As<IUsersReadRepository>().SingleInstance();
            builder.RegisterType<UsersWriteRepository>().As<IUsersWriteRepository>().SingleInstance();

            builder.Populate(services);
            this.ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
