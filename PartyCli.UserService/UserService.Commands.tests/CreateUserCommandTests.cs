﻿using System;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using SharedComponents.CommandsHandler.Enums;
using UserService.DataContracts.Requests.Commands;
using UserService.DataContracts.Requests.Repositories;
using UserService.DataContracts.Responses.Repositories;
using UserService.Repository;

namespace UserService.Commands.tests
{
    [TestFixture]
    public class CreateUserCommandTests
    {
        [Test]
        public void CreateUserCommand_Execute_ShouldReturnSuccess_WhenRepositoryResponseIsNotNull()
        {
            var iLoggerMock = new Mock<ILogger<CreateUserCommand>>();
            var userWriteRepositoryMock = new Mock<IUsersWriteRepository>();
            userWriteRepositoryMock.Setup(x => x.CreateUser(It.IsAny<CreateUserRepositoryRequest>()))
                .Returns(new CreateUserRepositoryResponse());

            var createUserCommand = new CreateUserCommand(userWriteRepositoryMock.Object, iLoggerMock.Object);

            var request = new CreateUserCommandRequest
            {
                Username = "Test user",
                Password = "Pasword"
            };

            var result = createUserCommand.Execute(request);

            Assert.AreEqual(CommandResponseType.Success, result.ResponseType);
            userWriteRepositoryMock.Verify(x => x.CreateUser(It.Is<CreateUserRepositoryRequest>(c => c.Password == request.Password && c.Username == request.Username)), Times.Once);
        }

        [Test]
        public void CreateUserCommand_Execute_ShouldReturnUnhandled_WhenRepositoryResponseNull()
        {
            var iLoggerMock = new Mock<ILogger<CreateUserCommand>>();
            var userWriteRepositoryMock = new Mock<IUsersWriteRepository>();
            userWriteRepositoryMock.Setup(x => x.CreateUser(It.IsAny<CreateUserRepositoryRequest>()))
                .Returns((CreateUserRepositoryResponse)null);

            var createUserCommand = new CreateUserCommand(userWriteRepositoryMock.Object, iLoggerMock.Object);

            var request = new CreateUserCommandRequest
            {
                Username = "Test user",
                Password = "Pasword"
            };

            var result = createUserCommand.Execute(request);

            Assert.AreEqual(CommandResponseType.Unhandled, result.ResponseType);
            Assert.NotNull(result.Error);
            Assert.AreEqual("Unable to create user", result.Error.ErrorMessage);
            userWriteRepositoryMock.Verify(x => x.CreateUser(It.Is<CreateUserRepositoryRequest>(c => c.Password == request.Password && c.Username == request.Username)), Times.Once);
        }
    }
}
