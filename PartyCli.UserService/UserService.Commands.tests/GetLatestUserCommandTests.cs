﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using SharedComponents.CommandsHandler.Enums;
using UserService.DataContracts.Requests.Commands;
using UserService.DataContracts.Requests.Repositories;
using UserService.DataContracts.Responses.Repositories;
using UserService.Repository;

namespace UserService.Commands.tests
{
    [TestFixture]
    public class GetLatestUserCommandTests
    {
        [Test]
        public void GetLatestUserCommand_Execute_ShouldReturnSuccess_WhenRepositoryResponseIsNotNull()
        {
            var response = new GetLatestUserRepositoryResponse
            {
                Id = 4,
                Password = "pass",
                Username = "username"
            };
            var iLoggerMock = new Mock<ILogger<GetLatestUserCommand>>();
            var userReadRepositoryMock = new Mock<IUsersReadRepository>();
            userReadRepositoryMock.Setup(x => x.GetLatestUser())
                .Returns(response);

            var getLatestUserCommand = new GetLatestUserCommand(userReadRepositoryMock.Object, iLoggerMock.Object);

            var request = new GetLatestUserCommandRequest();

            var result = getLatestUserCommand.Execute(request);

            Assert.AreEqual(CommandResponseType.Success, result.ResponseType);
            Assert.IsTrue(response.Id == result.Result.Id &&
                          response.Password == result.Result.Password &&
                          response.Username == result.Result.Username);
            userReadRepositoryMock.Verify(x => x.GetLatestUser(), Times.Once);
        }

        [Test]
        public void GetLatestUserCommand_Execute_ShouldReturnSuccess_WhenRepositoryResponseNull()
        {
            var iLoggerMock = new Mock<ILogger<GetLatestUserCommand>>();
            var userReadRepositoryMock = new Mock<IUsersReadRepository>();
            userReadRepositoryMock.Setup(x => x.GetLatestUser())
                .Returns((GetLatestUserRepositoryResponse)null);

            var getLatestUserCommand = new GetLatestUserCommand(userReadRepositoryMock.Object, iLoggerMock.Object);

            var request = new GetLatestUserCommandRequest();

            var result = getLatestUserCommand.Execute(request);

            Assert.AreEqual(CommandResponseType.Unhandled, result.ResponseType);
            Assert.NotNull(result.Error);
            Assert.AreEqual("Unable to get latest user.", result.Error.ErrorMessage);
            userReadRepositoryMock.Verify(x => x.GetLatestUser(), Times.Once);
        }
    }
}
