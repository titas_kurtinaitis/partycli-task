﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;
using UserService.DataContracts.Requests.Commands;
using UserService.DataContracts.Responses.Commands;
using UserService.Repository;

namespace UserService.Commands
{
    public class GetLatestUserCommand : ICommand<GetLatestUserCommandRequest, CommandResponse<GetLatesetUserCommandResponse>>

    {
        private readonly IUsersReadRepository usersReadRepository;
        private readonly ILogger<GetLatestUserCommand> logger;

        public GetLatestUserCommand(IUsersReadRepository usersReadRepository, ILogger<GetLatestUserCommand> logger)
        {
            this.logger = logger;
            this.usersReadRepository = usersReadRepository;
        }

        public CommandResponse<GetLatesetUserCommandResponse> Execute(GetLatestUserCommandRequest request)
        {
            var repositoryResponse = this.usersReadRepository.GetLatestUser();

            if (repositoryResponse != null)
            {
                this.logger.LogDebug("Latest user successfully found.");

                return new CommandResponse<GetLatesetUserCommandResponse>
                {
                    Result = new GetLatesetUserCommandResponse
                    {
                        Id = repositoryResponse.Id,
                        Password = repositoryResponse.Password,
                        Username = repositoryResponse.Username
                    },
                    ResponseType = CommandResponseType.Success
                };
            }

            this.logger.LogError("Error occured, wasn't able to get latest user.");

            return new CommandResponse<GetLatesetUserCommandResponse>
            {
                ResponseType = CommandResponseType.Unhandled,
                Error = new ErrorResult
                {
                    ErrorMessage = "Unable to get latest user."
                }
            };
        }
    }
}
