﻿using Microsoft.Extensions.Logging;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;
using UserService.DataContracts.Requests.Commands;
using UserService.DataContracts.Requests.Repositories;
using UserService.DataContracts.Responses.Commands;
using UserService.Repository;

namespace UserService.Commands
{
    public class CreateUserCommand : ICommand<CreateUserCommandRequest, CommandResponse<CreateUserCommandResponse>>
    {
        private readonly IUsersWriteRepository usersWriteRepository;
        private readonly ILogger<CreateUserCommand> logger;

        public CreateUserCommand(IUsersWriteRepository usersWriteRepository, ILogger<CreateUserCommand> logger)
        {
            this.usersWriteRepository = usersWriteRepository;
            this.logger = logger;
        }

        public CommandResponse<CreateUserCommandResponse> Execute(CreateUserCommandRequest request)
        {
            var repositoryRequest = new CreateUserRepositoryRequest
            {
                Username = request.Username,
                Password = request.Password
            };

            var repositoryResponse = this.usersWriteRepository.CreateUser(repositoryRequest);

            if (repositoryResponse != null)
            {
                this.logger.LogDebug($"User successfully created username: {request.Username}.");

                return new CommandResponse<CreateUserCommandResponse>
                {
                    Result = new CreateUserCommandResponse
                    {
                        Id = repositoryResponse.Id
                    },
                    ResponseType = CommandResponseType.Success
                };
            }

            this.logger.LogError("Error occured, wasn't able to create user.");

            return new CommandResponse<CreateUserCommandResponse>
            {
                ResponseType = CommandResponseType.Unhandled,
                Error = new ErrorResult
                {
                    ErrorMessage = "Unable to create user"
                }
            };
        }
    }
}
