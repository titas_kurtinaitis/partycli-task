﻿using LiteDB;
using Microsoft.Extensions.Logging;
using UserService.DataContracts.DomainModels;
using UserService.DataContracts.Requests.Repositories;
using UserService.DataContracts.Responses.Repositories;
using UserService.Repository.Constants;

namespace UserService.Repository
{
    public class UsersWriteRepository : IUsersWriteRepository
    {
        private readonly LiteDatabase db;
        private readonly ILogger<UsersWriteRepository> logger;

        public UsersWriteRepository(ILogger<UsersWriteRepository> logger)
        {
            this.logger = logger;
            this.db = new LiteDatabase("../Users.db");
        }

        public CreateUserRepositoryResponse CreateUser(CreateUserRepositoryRequest request)
        {
            var user = new User
            {
                Username = request.Username,
                Password = request.Password
            };

            var col = this.db.GetCollection<User>(RepositoryConstants.UsersCollectionName);

            this.logger.LogDebug($"Creating user with username: {request.Username}");

            col.Insert(user);
            col.EnsureIndex(x => x.Username);
            var insertedUser = col.FindOne(x => x.Username == user.Username);

            if (insertedUser != null)
            {
                this.logger.LogDebug($"Created user with username: {request.Username}");

                return new CreateUserRepositoryResponse
                {
                    Id = insertedUser.Id
                };
            }

            this.logger.LogError($"Creating user with username: {request.Username} was unsuccessful.");

            return null;
        }
    }
}
