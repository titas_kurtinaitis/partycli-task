﻿using System.Linq;
using LiteDB;
using Microsoft.Extensions.Logging;
using UserService.DataContracts.DomainModels;
using UserService.DataContracts.Responses.Repositories;
using UserService.Repository.Constants;

namespace UserService.Repository
{
    public class UsersReadRepository : IUsersReadRepository
    {
        private readonly LiteDatabase db;
        private readonly ILogger<UsersReadRepository> logger;

        public UsersReadRepository(ILogger<UsersReadRepository> logger)
        {
            this.db = new LiteDatabase("../Users.db");
            this.logger = logger;
        }

        public GetUserReadRepositoryResponse GetUserByUsername(string username)
        {
            this.logger.LogDebug($"Searching for user with username: {username}");
            var col = this.db.GetCollection<User>(RepositoryConstants.UsersCollectionName);
            var result = col.FindOne(x => x.Username == username);

            if (result != null)
            {
                this.logger.LogDebug($"User with username: {username} was found.");
                return new GetUserReadRepositoryResponse
                {
                    Id = result.Id,
                    Username = result.Username,
                    Password = result.Password
                };
            }
            this.logger.LogDebug($"User with username: {username} was not found.");
            return null;
        }

        public GetLatestUserRepositoryResponse GetLatestUser()
        {
            this.logger.LogDebug("Getting latest user.");
            var col = this.db.GetCollection<User>(RepositoryConstants.UsersCollectionName);
            var result = col.FindAll().OrderByDescending(x => x.Id).FirstOrDefault();

            if (result != null)
            {
                this.logger.LogDebug($"Latest user was found.");
                return new GetLatestUserRepositoryResponse
                {
                    Id = result.Id,
                    Username = result.Username,
                    Password = result.Password
                };
            }
            this.logger.LogDebug("Latest user was not found.");
            return null;
        }
    }
}

