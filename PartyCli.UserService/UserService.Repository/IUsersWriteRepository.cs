﻿using UserService.DataContracts.Requests.Repositories;
using UserService.DataContracts.Responses.Repositories;

namespace UserService.Repository
{
    public interface IUsersWriteRepository
    {
        CreateUserRepositoryResponse CreateUser(CreateUserRepositoryRequest request);
    }
}
