﻿using UserService.DataContracts.Responses.Repositories;

namespace UserService.Repository
{
    public interface IUsersReadRepository
    {
        GetUserReadRepositoryResponse GetUserByUsername(string username);

        GetLatestUserRepositoryResponse GetLatestUser();
    }
}