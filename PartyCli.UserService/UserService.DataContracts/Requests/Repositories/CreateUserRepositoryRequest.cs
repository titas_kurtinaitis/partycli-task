﻿namespace UserService.DataContracts.Requests.Repositories
{
    public class CreateUserRepositoryRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
