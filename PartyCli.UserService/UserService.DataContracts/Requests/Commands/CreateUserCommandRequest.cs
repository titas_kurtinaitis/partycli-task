﻿namespace UserService.DataContracts.Requests.Commands
{
    public class CreateUserCommandRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
