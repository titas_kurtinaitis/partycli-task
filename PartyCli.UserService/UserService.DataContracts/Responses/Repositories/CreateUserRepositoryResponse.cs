﻿namespace UserService.DataContracts.Responses.Repositories
{
    public class CreateUserRepositoryResponse
    {
        public int Id { get; set; }
    }
}
