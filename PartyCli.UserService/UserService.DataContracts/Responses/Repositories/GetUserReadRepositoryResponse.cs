﻿namespace UserService.DataContracts.Responses.Repositories
{
    public class GetUserReadRepositoryResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
