﻿namespace UserService.DataContracts.Responses.Repositories
{
    public class GetLatestUserRepositoryResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
