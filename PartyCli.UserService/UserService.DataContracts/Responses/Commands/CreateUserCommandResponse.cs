﻿namespace UserService.DataContracts.Responses.Commands
{
    public class CreateUserCommandResponse
    {
        public int Id { get; set; }
    }
}
