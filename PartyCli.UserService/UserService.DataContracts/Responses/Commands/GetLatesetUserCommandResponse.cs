﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserService.DataContracts.Responses.Commands
{
    public class GetLatesetUserCommandResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
