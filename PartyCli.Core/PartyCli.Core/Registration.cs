﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using Autofac.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PartyCli.Core.CommandsExecutor;
using PartyCli.Core.CommandsExecutor.Commands;
using PartyCli.Core.CommandsHandler;
using PartyCli.Core.ConfigurationContracts;
using SharedComponents.ServerServiceRestClient;
using SharedComponents.ServiceDiscoveryRestClient;
using SharedComponents.ServiceDiscoveryRestClient.Config;
using SharedComponents.UserServiceRestClient;
using UserService.Repository;

namespace PartyCli.Core
{
    public class Registration
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public static IContainer ConfigureServices()
        {

            var services = new ServiceCollection();

            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appSettings.json", optional: true, reloadOnChange: true)
                .Build();
            services.AddSingleton<IConfiguration>(configuration);

            var builder = new ContainerBuilder();

            builder.RegisterType<CommandsHandler.CommandsHandler>().As<ICommandsHandler>().SingleInstance();
            builder.RegisterType<CommandsExecutor.CommandsExecutor>().As<ICommandsExecutor>().SingleInstance();
            builder.RegisterType<StoreUserCredentialsCommand>().As<IStoreUserCredentialsCommand>().SingleInstance();
            builder.RegisterType<GetServersListCommand>().As<IGetServersListCommand>().SingleInstance();
            builder.RegisterType<Printer>().As<IPrinter>().SingleInstance();

            services.Configure<ServiceDiscoveryConfiguration>(configuration.GetSection(nameof(ServiceDiscoveryConfiguration)));
            services.Configure<ExternalApiConfigurations>(configuration.GetSection(nameof(ExternalApiConfigurations)));


            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));


            builder.RegisterType<ServiceDiscoveryRestClient>().As<IServiceDiscoveryRestClient>().SingleInstance();
            builder.RegisterType<UserServiceRestClient>().As<IUserServiceRestClient>().SingleInstance();
            builder.RegisterType<ServerServiceRestClient>().As<IServerServiceRestClient>().SingleInstance();


            builder.Populate(services);

            return builder.Build();
        }
    }
}
