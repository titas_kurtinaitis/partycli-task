﻿using System;
using Autofac;
using Microsoft.Extensions.Logging;
using PartyCli.Core.CommandsHandler;
using PartyCli.Core.Exceptions;


namespace PartyCli.Core
{
    class Program
    {
        static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            Container = Registration.ConfigureServices();
            var commandsHandler = Container.Resolve<ICommandsHandler>();
            var logger = Container.Resolve<ILogger<Program>>();

            try
            {
                commandsHandler.ReceiveArguments(args);
            }
            catch (ProgramException ex)
            {
                Console.WriteLine(ex);
                logger.LogError(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                logger.LogError(ex.ToString());
            }


            Console.ReadLine();
        }
    }
}
