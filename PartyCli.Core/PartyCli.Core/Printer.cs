﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedComponents.ServerServiceRestClient.DataContracts;

namespace PartyCli.Core.CommandsExecutor
{
    public class Printer : IPrinter
    {
        public void ShowServers(IEnumerable<Server> servers)
        {
            foreach (var server in servers)
            {
                Console.WriteLine(server.Name);
            }

            Console.WriteLine($"Servers count: {servers.Count()}.");
        }
    }
}
