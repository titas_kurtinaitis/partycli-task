﻿using System;
using System.Collections.Generic;
using System.Text;
using PartyCli.Core.CommandsExecutor.Commands;
using SharedComponents.ServerServiceRestClient.DataContracts;

namespace PartyCli.Core
{
    public interface IPrinter
    {
        void ShowServers(IEnumerable<Server> servers);
    }
}
