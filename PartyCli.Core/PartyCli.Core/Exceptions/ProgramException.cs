﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PartyCli.Core.Exceptions
{
    public class ProgramException : Exception
    {
        public ProgramException()
        {
        }

        public ProgramException(string message)
            : base(message)
        {
        }

        public ProgramException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
