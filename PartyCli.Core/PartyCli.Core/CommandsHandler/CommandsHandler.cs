﻿using System;
using PartyCli.Core.CommandsExecutor;
using PartyCli.Core.Exceptions;

namespace PartyCli.Core.CommandsHandler
{
    public class CommandsHandler : ICommandsHandler
    {
        private readonly ICommandsExecutor commandsExecutor;

        public CommandsHandler(ICommandsExecutor commandsExecutor)
        {
            this.commandsExecutor = commandsExecutor;
        }

        public void ReceiveArguments(string[] args)
        {
            if (args.Length < 0)
            {
                throw new ProgramException("No arguments were given.");
            }

            var isParseSuccess = Enum.TryParse<AvailableCommandsEnum>(args[0], out var commandToExecute);

            if (isParseSuccess == false)
            {
                throw new ProgramException("Unavailable command tried to be executed.");
            }

            this.commandsExecutor.ExecuteCommand(commandToExecute, args);
        }
    }
}
