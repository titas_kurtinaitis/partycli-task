﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PartyCli.Core.CommandsHandler
{
    public interface ICommandsHandler
    {
        void ReceiveArguments(string[] args);
    }
}
