﻿namespace PartyCli.Core.CommandsExecutor
{
    public interface ICommand
    {
        void Execute(string [] args);
    }
}
