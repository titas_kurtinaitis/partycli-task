﻿using System.Threading.Tasks;

namespace PartyCli.Core.CommandsExecutor
{
    public interface ICommandsExecutor
    {
        void ExecuteCommand(AvailableCommandsEnum commandType, string[] args);
    }
}
