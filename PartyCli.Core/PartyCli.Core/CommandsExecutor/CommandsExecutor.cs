﻿using System.Threading.Tasks;
using PartyCli.Core.CommandsExecutor.Commands;
using PartyCli.Core.CommandsHandler;
using PartyCli.Core.Exceptions;

namespace PartyCli.Core.CommandsExecutor
{
    public class CommandsExecutor : ICommandsExecutor
    {
        private readonly IStoreUserCredentialsCommand storeUserCredentialsCommand;
        private readonly IGetServersListCommand getServersListCommand;

        public CommandsExecutor(IStoreUserCredentialsCommand storeUserCredentialsCommand, IGetServersListCommand serversListCommand)
        {
            getServersListCommand = serversListCommand;
            this.storeUserCredentialsCommand = storeUserCredentialsCommand;
        }

        public void ExecuteCommand(AvailableCommandsEnum commandType, string[] args)
        {
            switch (commandType)
            {
                case AvailableCommandsEnum.config:
                    this.storeUserCredentialsCommand.Execute(args);
                    break;
                case AvailableCommandsEnum.server_list:
                    this.getServersListCommand.Execute(args);
                    break;
                default:
                    return;
            }
        }
    }
}
