﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PartyCli.Core.ConfigurationContracts;
using PartyCli.Core.Exceptions;
using SharedComponents.ServerServiceRestClient;
using SharedComponents.ServerServiceRestClient.DataContracts;
using SharedComponents.UserServiceRestClient;

namespace PartyCli.Core.CommandsExecutor.Commands
{
    public class GetServersListCommand : IGetServersListCommand
    {
        private readonly HttpClient httpClient;
        private readonly string apiUrl;
        private readonly string authenticationUrl;
        private readonly IUserServiceRestClient userServiceRestClient;
        private readonly IServerServiceRestClient serverServiceRestClient;
        private readonly ILogger<GetServersListCommand> logger;
        private readonly IPrinter printer;

        public GetServersListCommand(IOptions<ExternalApiConfigurations> configurations, IUserServiceRestClient userServiceRestClient, IServerServiceRestClient serverServiceRestClient, ILogger<GetServersListCommand> logger, IPrinter printer)
        {
            this.printer = printer;
            this.logger = logger;
            this.serverServiceRestClient = serverServiceRestClient;
            this.userServiceRestClient = userServiceRestClient;
            this.httpClient = new HttpClient(new HttpClientHandler
            {
                UseProxy = false
            });
            apiUrl = configurations.Value.ApiUrl ?? throw new ArgumentNullException(nameof(ExternalApiConfigurations));
            authenticationUrl = configurations.Value.AuthenticationUrl ?? throw new ArgumentNullException(nameof(ExternalApiConfigurations));
        }

        public void Execute(string[] args)
        {
            IEnumerable<Server> serversResult;
            if (args.Length > 1 && args[1] == "--local")
            {
                serversResult = GetServersFromLocalDataStore();
                this.printer.ShowServers(serversResult);

                return;
            }
            serversResult = GetServersFromApi();
            this.printer.ShowServers(serversResult);
            SaveServersToLocalDataStore(serversResult);
        }

        private void SaveServersToLocalDataStore(IEnumerable<Server> servers)
        {
            var response = this.serverServiceRestClient.CreateServersListAsync(new CreateServersListRequest
            {
                Servers = servers
            }).Result;

            if (response.IsSuccess)
            {
                this.logger.LogDebug("Servers successfuly store to local datastore.");

                return;
            }

            this.logger.LogError("Unable to store servers list. Check logs for additional information.");
            throw new ProgramException("Unable to store servers list.");
        }

        private IEnumerable<Server> GetServersFromLocalDataStore()
        {
            var response = this.serverServiceRestClient.GetServersListAsync().Result;

            if (response.IsSuccess)
            {
                this.logger.LogDebug("Servers successfuly store to local datastore.");

                return response.Servers;
            }

            this.logger.LogError("Unable to store servers list. Check logs for additional information.");
            throw new ProgramException("Unable to store servers list.");
        }

        private IEnumerable<Server> GetServersFromApi()
        {
            var token = GetToken();

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token);
            var response = this.httpClient.GetAsync(apiUrl).Result;

            if (response.IsSuccessStatusCode)
            {
                var stringContent = response.Content.ReadAsStringAsync().Result;

                var result = JsonConvert.DeserializeObject<IEnumerable<Server>>(stringContent).ToList();

                this.logger.LogDebug("Server list was successful returned from API.");

                return result;
            }


            this.logger.LogError($"Could not get servers info. Error code: {response.StatusCode.ToString()}. Reason {response.ReasonPhrase}");
            throw new ProgramException("Unable to get Servers List. Check logs for more information.");

        }

        private string GetToken()
        {
            var userRestClientResponse = this.userServiceRestClient.GetLatestUserAsync().Result;

            if (!userRestClientResponse.IsSuccess)
            {
                this.logger.LogError("Cannot get user from datastore.");
                throw new ProgramException("Cannot get user from datastore. Check logs for additional information");
            }

            var postBody = JsonConvert.SerializeObject(new { username = userRestClientResponse.User.Username, password = userRestClientResponse.User.Password });

            var response = this.httpClient.PostAsync(this.authenticationUrl, new StringContent(postBody, Encoding.UTF8, "application/json")).Result;

            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;

                var tokkenObject = JsonConvert.DeserializeObject<dynamic>(result);

                this.logger.LogDebug("Access token successful returned");

                return tokkenObject.token;
            }

            this.logger.LogDebug("Access token was not returned");
            throw new ProgramException("Unable to get access token. Check logs for more information.");
        }
    }
}
