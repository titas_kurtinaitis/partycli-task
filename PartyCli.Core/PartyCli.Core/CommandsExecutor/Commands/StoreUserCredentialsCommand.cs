﻿using System;
using Microsoft.Extensions.Logging;
using PartyCli.Core.Exceptions;
using SharedComponents.UserServiceRestClient;
using SharedComponents.UserServiceRestClient.DataContracts;

namespace PartyCli.Core.CommandsExecutor.Commands
{
    public class StoreUserCredentialsCommand : IStoreUserCredentialsCommand
    {
        private readonly IUserServiceRestClient userServiceRestClient;
        private readonly ILogger<StoreUserCredentialsCommand> logger;

        public StoreUserCredentialsCommand(IUserServiceRestClient userServiceRestClient, ILogger<StoreUserCredentialsCommand> logger)
        {
            this.logger = logger;
            this.userServiceRestClient = userServiceRestClient;
        }

        public void Execute(string[] args)
        {
            if (!AreParamsValid(args))
            {
                this.logger.LogError("Invalid parameters were given.");
                throw new ProgramException("Parameters are invalid. Check logs for more information.");
            }

            var result = this.userServiceRestClient.CreateUserAsync(new CreateUserRequest
            {
                Username = args[2],
                Password = args[4]
            }).Result;

            if (!result.IsSuccess)
            {
                this.logger.LogError($"Unable to create user with username: {args[2]}.");
                throw new ProgramException($"Unable to create user with username: {args[2]}. Check logs for more information.");
            }

            this.logger.LogDebug($"User: {args[2]} created successfuly.");
            Console.WriteLine($"User: {args[2]} created successfuly.");
        }

        private bool AreParamsValid(string[] args)
        {
            return args.Length == 5 &&
                   args[1] == "--username" &&
                   args[3] == "--password";
        }
    }
}
