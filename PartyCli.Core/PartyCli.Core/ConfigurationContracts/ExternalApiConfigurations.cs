﻿namespace PartyCli.Core.ConfigurationContracts
{
    public class ExternalApiConfigurations
    {
        public string AuthenticationUrl { get; set; }
        public string ApiUrl { get; set; }
    }
}
