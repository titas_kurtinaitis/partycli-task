# PARTY CLI

## How to run project:

1. Run all microservices with "dotnet run" command. Microservices: UserService, ServiceDiscoveryService, ServerService
2. To build PartyCli.Core make sure you added LocalNugetStorage as nuget storage in visual studio.
3. Main application entry exists in PartyCli.Core

### Unfortunately things which are missing due to limited time this week:

1. No authentication for microservices. They are just open rest apis.
2. Only few unit tests in UserService (just didn't have enough time to write more).
3. Global restClient handler for microservices. It would make service's rest clients less polute.

Hope you enjoy, Have a good day.
