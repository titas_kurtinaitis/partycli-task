﻿using Microsoft.AspNetCore.Mvc;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;

namespace SharedComponents.ResponseHandler
{
    public class ApiController : ControllerBase
    {
        [NonAction]
        public IActionResult CommandErrorResponse<T>(CommandResponse<T> response)
        {
            switch (response.ResponseType)
            {
                case CommandResponseType.NotFound:
                    return this.StatusCode(404, (object) response.Error);
                case CommandResponseType.BusinessLogicValidation:
                    return this.StatusCode(400, (object) response.Error);
                case CommandResponseType.Unhandled:
                    return (IActionResult) this.StatusCode(500, (object) response.Error);
                default:
                    return (IActionResult) this.StatusCode(500, (object) new ErrorResult{ErrorMessage = "Internal Server Error"});
            }
        }
    }
}
