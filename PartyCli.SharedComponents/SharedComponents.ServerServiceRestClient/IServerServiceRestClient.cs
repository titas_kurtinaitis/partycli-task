﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SharedComponents.ServerServiceRestClient.DataContracts;

namespace SharedComponents.ServerServiceRestClient
{
    public interface IServerServiceRestClient
    {
        Task<CreateServersListResponse> CreateServersListAsync(CreateServersListRequest request);
        Task<GetServersListResponse> GetServersListAsync();
    }
}
