﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SharedComponents.ServerServiceRestClient.DataContracts;
using SharedComponents.ServiceDiscoveryRestClient;
using SharedComponents.ServiceDiscoveryRestClient.DataContracts;

namespace SharedComponents.ServerServiceRestClient
{
    public class ServerServiceRestClient : IServerServiceRestClient
    {
        private readonly IServiceDiscoveryRestClient serviceDiscoveryRestClient;
        private const string ServiceName = "server-service";
        private readonly HttpClient httpClient;
        private readonly ILogger<ServerServiceRestClient> logger;

        public ServerServiceRestClient(IServiceDiscoveryRestClient serviceDiscoveryRestClient, ILogger<ServerServiceRestClient> logger)
        {
            this.logger = logger;
            this.serviceDiscoveryRestClient = serviceDiscoveryRestClient;
            this.httpClient = new HttpClient(new HttpClientHandler
            {
                UseProxy = false
            });
        }

        public async Task<CreateServersListResponse> CreateServersListAsync(CreateServersListRequest request)
        {
            var serviceDiscoveryResponse = this.serviceDiscoveryRestClient.GetServiceUrl(new GetServiceUrlRequest
            {
                ServiceName = ServiceName
            });

            if (!serviceDiscoveryResponse.IsSuccess)
            {
                this.logger.LogError("Unable to resolve service url through service discovery.");
                return new CreateServersListResponse
                {
                    IsSuccess = false
                };
            }

            this.logger.LogDebug($"Server service url were resolved, url: {serviceDiscoveryResponse.Content.ServiceUrl}");

            var url = serviceDiscoveryResponse.Content.ServiceUrl + "/servers";
            var requestAsJson = JsonConvert.SerializeObject(request);

            var responseMessage = await this.httpClient.PostAsync(url, new StringContent(requestAsJson, Encoding.UTF8,"application/json"));

            if (!responseMessage.IsSuccessStatusCode)
            {
                this.logger.LogError("Http request to create servers list failed.");
                return new CreateServersListResponse
                {
                    IsSuccess = false
                };
            }

            this.logger.LogDebug("Http request to create servers list succeeded.");

            return new CreateServersListResponse
            {
                IsSuccess = true
            };
        }

        public async Task<GetServersListResponse> GetServersListAsync()
        {
            var serviceDiscoveryResponse = this.serviceDiscoveryRestClient.GetServiceUrl(new GetServiceUrlRequest
            {
                ServiceName = ServiceName
            });

            if (!serviceDiscoveryResponse.IsSuccess)
            {
                this.logger.LogError("Unable to resolve service url through service discovery.");
                return new GetServersListResponse
                {
                    IsSuccess = false
                };
            }

            var url = serviceDiscoveryResponse.Content.ServiceUrl + "/servers";

            var responseMessage = await this.httpClient.GetAsync(url);

            if (!responseMessage.IsSuccessStatusCode)
            {
                this.logger.LogError("Http request to get servers list failed.");
                return new GetServersListResponse
                {
                    IsSuccess = false
                };
            }

            var responseMessageContent = await responseMessage.Content.ReadAsStringAsync();
            var responseContent = JsonConvert.DeserializeObject<GetServersListResponse>(responseMessageContent);
            this.logger.LogDebug("Http request to get servers list succeeded.");

            return new GetServersListResponse
            {
                Servers = responseContent.Servers,
                IsSuccess = true
            };
        }
    }
}
