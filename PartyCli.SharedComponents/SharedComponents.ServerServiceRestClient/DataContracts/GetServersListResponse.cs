﻿using System;
using System.Collections.Generic;

namespace SharedComponents.ServerServiceRestClient.DataContracts
{
    public class GetServersListResponse
    {
        public IEnumerable<Server> Servers { get; set; }

        public bool IsSuccess { get; set; }
    }
}
