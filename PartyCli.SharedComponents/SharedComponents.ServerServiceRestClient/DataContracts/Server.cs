﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedComponents.ServerServiceRestClient.DataContracts
{
    public class Server
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Distance { get; set; }
    }
}
