﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedComponents.ServerServiceRestClient.DataContracts
{
    public class CreateServersListResponse
    {
        public bool IsSuccess { get; set; }
    }
}
