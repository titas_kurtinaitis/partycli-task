﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedComponents.ServerServiceRestClient.DataContracts
{
    public class CreateServersListRequest
    {
        public IEnumerable<Server> Servers { get; set; }
    }
}
