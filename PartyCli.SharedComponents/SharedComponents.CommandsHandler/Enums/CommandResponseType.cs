﻿namespace SharedComponents.CommandsHandler.Enums
{
    public enum CommandResponseType
    {
        Success,
        NotFound,
        BusinessLogicValidation,
        Unhandled
    }
}
