﻿namespace SharedComponents.CommandsHandler
{
    public interface ICommand<in TRequest, out TResult>
    {
        TResult Execute(TRequest request);
    }
}
