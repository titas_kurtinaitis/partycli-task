﻿using SharedComponents.CommandsHandler.Enums;

namespace SharedComponents.CommandsHandler
{
    public class CommandResponse<T>
    {
        public T Result { get; set; }
        public CommandResponseType ResponseType { get; set; }
        public ErrorResult Error { get; set; }
    }
}
