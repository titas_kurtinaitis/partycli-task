﻿using SharedComponents.ServiceDiscoveryRestClient.DataContracts;

namespace SharedComponents.ServiceDiscoveryRestClient
{
    public interface IServiceDiscoveryRestClient
    {
        HttpResponse<ServiceDiscoveryResponse> GetServiceUrl(GetServiceUrlRequest request);
    }
}
