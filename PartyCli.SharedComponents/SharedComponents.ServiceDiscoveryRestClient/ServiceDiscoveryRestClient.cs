﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SharedComponents.ServiceDiscoveryRestClient.Config;
using SharedComponents.ServiceDiscoveryRestClient.DataContracts;

namespace SharedComponents.ServiceDiscoveryRestClient
{
    public class ServiceDiscoveryRestClient : IServiceDiscoveryRestClient
    {
        private readonly HttpClient httpClient;
        private readonly string baseUrl;
        private readonly ILogger<ServiceDiscoveryRestClient> logger;

        public ServiceDiscoveryRestClient(IOptions<ServiceDiscoveryConfiguration> serviceDiscoveryConfig, ILogger<ServiceDiscoveryRestClient> logger)
        {
            this.logger = logger;
            this.httpClient = new HttpClient(new HttpClientHandler
            {
                UseProxy = false
            });
            this.baseUrl = serviceDiscoveryConfig.Value.ServiceDiscoveryUrl ?? throw new ArgumentNullException(nameof(serviceDiscoveryConfig));
        }

        public HttpResponse<ServiceDiscoveryResponse> GetServiceUrl(GetServiceUrlRequest request)
        {
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(baseUrl + $"/service-discovery/{request.ServiceName}")
            };

            var responseMessage = this.httpClient.SendAsync(requestMessage).GetAwaiter().GetResult();

            var httpResponse = new HttpResponse<ServiceDiscoveryResponse>
            {
                StatusCode = responseMessage.StatusCode
            };

            if (httpResponse.IsSuccess)
            {
                this.logger.LogError("Unable to resolve url.");
                var responseMessageContent = responseMessage.Content.ReadAsStringAsync().Result;
                var responseContent = JsonConvert.DeserializeObject<ServiceDiscoveryResponse>(responseMessageContent);
                httpResponse.Content = responseContent;
            }

            this.logger.LogDebug("Url resolved successfuly.");
            return httpResponse;
        }
    }
}
