﻿namespace SharedComponents.ServiceDiscoveryRestClient.DataContracts
{
    public class ServiceDiscoveryResponse
    {
        public string ServiceUrl { get; set; }
    }
}
