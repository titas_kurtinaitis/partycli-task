﻿using System.Net;

namespace SharedComponents.ServiceDiscoveryRestClient.DataContracts
{
    public class HttpResponse<T>
    {
        public bool IsSuccess => ((int)this.StatusCode >= 200) && ((int)this.StatusCode <= 299);

        public T Content { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}
