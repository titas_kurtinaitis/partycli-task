﻿namespace SharedComponents.ServiceDiscoveryRestClient.DataContracts
{
    public class GetServiceUrlRequest
    {
        public string ServiceName { get; set; }
    }
}
