﻿namespace SharedComponents.ServiceDiscoveryRestClient.Config
{
    public class ServiceDiscoveryConfiguration
    {
        public string ServiceDiscoveryUrl { get; set; }
    }
}
