﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SharedComponents.ServiceDiscoveryRestClient;
using SharedComponents.ServiceDiscoveryRestClient.DataContracts;
using SharedComponents.UserServiceRestClient.DataContracts;

namespace SharedComponents.UserServiceRestClient
{
    public class UserServiceRestClient : IUserServiceRestClient
    {
        private readonly HttpClient httpClient;
        private readonly IServiceDiscoveryRestClient serviceDiscoveryRestClient;
        private readonly ILogger<UserServiceRestClient> logger;
        private const string ServiceName = "user-service";

        public UserServiceRestClient(IServiceDiscoveryRestClient serviceDiscoveryRestClient, ILogger<UserServiceRestClient> logger)
        {
            this.logger = logger;
            this.serviceDiscoveryRestClient = serviceDiscoveryRestClient;
            this.httpClient = new HttpClient(new HttpClientHandler
            {
                UseProxy = false
            });
        }

        public async Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request)
        {
            var serviceDiscoveryResponse = this.serviceDiscoveryRestClient.GetServiceUrl(new GetServiceUrlRequest
            {
                ServiceName = ServiceName
            });

            if (!serviceDiscoveryResponse.IsSuccess)
            {
                this.logger.LogError("Unable to resolve service url through service discovery.");

                return new CreateUserResponse
                {
                    IsSuccess = false
                };
            }

            this.logger.LogDebug($"User service url were resolved, url: {serviceDiscoveryResponse.Content.ServiceUrl}");

            var url = serviceDiscoveryResponse.Content.ServiceUrl + "/users";
            var requestAsJson = JsonConvert.SerializeObject(request);

            var responseMessage = await this.httpClient.PostAsync(url, new StringContent(requestAsJson, Encoding.UTF8,"application/json"));

            if (!responseMessage.IsSuccessStatusCode)
            {
                this.logger.LogError("Http request to create user if not exists failed.");

                return new CreateUserResponse
                {
                    IsSuccess = false
                };
            }

            this.logger.LogDebug("Http request to create user if not exists succeeded.");

            return new CreateUserResponse
            {
                IsSuccess = true
            };
        }

        public async Task<GetLatestUserResponse> GetLatestUserAsync()
        {
            var serviceDiscoveryResponse = this.serviceDiscoveryRestClient.GetServiceUrl(new GetServiceUrlRequest
            {
                ServiceName = ServiceName
            });

            if (!serviceDiscoveryResponse.IsSuccess)
            {
                this.logger.LogError("Unable to resolve service url through service discovery.");

                return new GetLatestUserResponse
                {
                    IsSuccess = false
                };
            }

            this.logger.LogDebug($"User service url were resolved, url: {serviceDiscoveryResponse.Content.ServiceUrl}");

            var url = serviceDiscoveryResponse.Content.ServiceUrl + "/users/queries/get-latest-user";

            var responseMessage = await this.httpClient.GetAsync(url);

            if (!responseMessage.IsSuccessStatusCode)
            {
                this.logger.LogError("Http request to get latest user failed.");

                return new GetLatestUserResponse
                {
                    IsSuccess = false
                };
            }

            var responseMessageContent = await responseMessage.Content.ReadAsStringAsync();
            var responseContent = JsonConvert.DeserializeObject<User>(responseMessageContent);

            this.logger.LogDebug("Http request to create user if not exists succeeded.");

            return new GetLatestUserResponse
            {
                User = responseContent,
                IsSuccess = true
            };
        }
    }
}
