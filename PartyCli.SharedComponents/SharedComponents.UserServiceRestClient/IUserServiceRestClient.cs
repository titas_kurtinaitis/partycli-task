﻿using System.Threading.Tasks;
using SharedComponents.UserServiceRestClient.DataContracts;

namespace SharedComponents.UserServiceRestClient
{
    public interface IUserServiceRestClient
    {
        Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request);

        Task<GetLatestUserResponse> GetLatestUserAsync();
    }
}
