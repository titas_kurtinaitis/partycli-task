﻿namespace SharedComponents.UserServiceRestClient.DataContracts
{
    public class CreateUserResponse
    {
        public bool IsSuccess { get; set; }
    }
}
