﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedComponents.UserServiceRestClient.DataContracts
{
    public class GetLatestUserResponse
    {
        public User User { get; set; }

        public bool IsSuccess { get; set; }
    }
}
