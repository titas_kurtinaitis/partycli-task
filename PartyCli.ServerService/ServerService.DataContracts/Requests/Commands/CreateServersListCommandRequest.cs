﻿using System.Collections.Generic;
using ServerService.DataContracts.DomainModels;

namespace ServerService.DataContracts.Requests.Commands
{
    public class CreateServersListCommandRequest
    {
        public IEnumerable<Server> Servers { get; set; }
    }
}
