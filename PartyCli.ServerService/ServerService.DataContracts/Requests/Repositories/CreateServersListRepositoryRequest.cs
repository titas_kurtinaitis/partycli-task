﻿using System.Collections.Generic;
using ServerService.DataContracts.DomainModels;

namespace ServerService.DataContracts.Requests.Repositories
{
    public class CreateServersListRepositoryRequest
    {
        public IEnumerable<Server> Servers { get; set; }
    }
}
