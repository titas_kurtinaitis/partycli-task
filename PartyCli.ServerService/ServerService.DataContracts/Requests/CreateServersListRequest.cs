﻿using System.Collections.Generic;
using ServerService.DataContracts.DomainModels;

namespace ServerService.DataContracts.Requests
{
    public class CreateServersListRequest
    {
       public IEnumerable<Server> Servers { get; set; }
    }
}
