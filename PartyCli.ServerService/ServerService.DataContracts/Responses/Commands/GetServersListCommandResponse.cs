﻿using System.Collections.Generic;
using ServerService.DataContracts.DomainModels;

namespace ServerService.DataContracts.Responses.Commands
{
    public class GetServersListCommandResponse
    {
        public IEnumerable<Server> Servers { get; set; }
    }
}
