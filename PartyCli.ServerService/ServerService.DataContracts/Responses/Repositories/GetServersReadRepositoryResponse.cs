﻿using System.Collections.Generic;
using ServerService.DataContracts.DomainModels;

namespace ServerService.DataContracts.Responses.Repositories
{
    public class GetServersReadRepositoryResponse
    {
        public IEnumerable<Server> Servers { get; set; }
    }
}
