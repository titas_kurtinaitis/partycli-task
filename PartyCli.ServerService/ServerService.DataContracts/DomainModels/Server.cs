﻿namespace ServerService.DataContracts.DomainModels
{
    public class Server
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Distance { get; set; }
    }
}
