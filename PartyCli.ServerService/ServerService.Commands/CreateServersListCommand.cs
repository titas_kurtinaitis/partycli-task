﻿using Microsoft.Extensions.Logging;
using ServerService.DataContracts.Requests.Commands;
using ServerService.DataContracts.Requests.Repositories;
using ServerService.DataContracts.Responses.Commands;
using ServerService.Repository;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;

namespace ServerService.Commands
{
    public class CreateServersListCommand : ICommand<CreateServersListCommandRequest, CommandResponse<CreateServersListCommandResponse>>
    {
        private readonly IServersWriteRepository serversWriteRepository;
        private readonly ILogger<CreateServersListCommand> logger;

        public CreateServersListCommand(IServersWriteRepository serversWriteRepository, ILogger<CreateServersListCommand> logger)
        {
            this.logger = logger;
            this.serversWriteRepository = serversWriteRepository;
        }

        public CommandResponse<CreateServersListCommandResponse> Execute(CreateServersListCommandRequest request)
        {
            var repositoryRequest = new CreateServersListRepositoryRequest
            {
                Servers = request.Servers
            };

            var repositoryResult = this.serversWriteRepository.CreateServersList(repositoryRequest);

            if (repositoryResult != null)
            {
                this.logger.LogDebug("Creation of servers list were successful.");
                return new CommandResponse<CreateServersListCommandResponse>
                {
                    ResponseType = CommandResponseType.Success,
                    Result = new CreateServersListCommandResponse()
                };
            }

            this.logger.LogError("Unable to create servers list.");

            return new CommandResponse<CreateServersListCommandResponse>
            {
                ResponseType = CommandResponseType.Unhandled,
                Error = new ErrorResult
                {
                    ErrorMessage = "Failed to write servers list."
                }
            };
        }
    }
}
