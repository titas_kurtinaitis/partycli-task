﻿using Microsoft.Extensions.Logging;
using ServerService.DataContracts.Requests.Commands;
using ServerService.DataContracts.Responses.Commands;
using ServerService.Repository;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;

namespace ServerService.Commands
{
    public class GetServersListCommand : ICommand<GetServersListCommandRequest, CommandResponse<GetServersListCommandResponse>>
    {
        private readonly IServersReadRepository serversReadRepository;
        private readonly ILogger<GetServersListCommand> logger;

        public GetServersListCommand(IServersReadRepository serversReadRepository, ILogger<GetServersListCommand> logger)
        {
            this.logger = logger;
            this.serversReadRepository = serversReadRepository;
        }

        public CommandResponse<GetServersListCommandResponse> Execute(GetServersListCommandRequest request)
        {
            var result = this.serversReadRepository.GetServersList();

            if (result != null)
            {
                this.logger.LogDebug("Servers were found.");
                return new CommandResponse<GetServersListCommandResponse>
                {
                    Result = new GetServersListCommandResponse
                    {
                        Servers = result.Servers
                    },
                    ResponseType = CommandResponseType.Success
                };
            }

            this.logger.LogError("Retrieval of servers list was unsuccessful.");
            return new CommandResponse<GetServersListCommandResponse>
            {
                ResponseType = CommandResponseType.Unhandled,
                Error = new ErrorResult
                {
                    ErrorMessage = "Unable to retrieve Servers List."
                }
            };
        }
    }
}
