﻿using LiteDB;
using Microsoft.Extensions.Logging;
using ServerService.DataContracts.DomainModels;
using ServerService.DataContracts.Responses.Repositories;
using ServerService.Repository.Constants;

namespace ServerService.Repository
{
    public class ServersReadRepository : IServersReadRepository
    {
        private readonly LiteDatabase db;
        private readonly ILogger<ServersReadRepository> logger;

        public ServersReadRepository(ILogger<ServersReadRepository> logger)
        {
            this.logger = logger;
            this.db = new LiteDatabase("../Servers.db");
        }

        public GetServersReadRepositoryResponse GetServersList()
        {
            this.logger.LogDebug("Started getting servers list.");
            var col = this.db.GetCollection<Server>(RepositoryConstants.ServersCollectionName);
            var result = col.FindAll();

            if (result != null)
            {
                this.logger.LogDebug("Servers were found.");
                return new GetServersReadRepositoryResponse
                {
                   Servers = result
                };
            }
            this.logger.LogError("Read of servers were unsuccessful.");
            return null;
        }
    }
}
