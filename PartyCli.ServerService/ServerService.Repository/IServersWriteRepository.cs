﻿using ServerService.DataContracts.Requests.Repositories;
using ServerService.DataContracts.Responses.Repositories;

namespace ServerService.Repository
{
    public interface IServersWriteRepository
    {
        CreateServersRepositoryResponse CreateServersList(CreateServersListRepositoryRequest request);
    }
}
