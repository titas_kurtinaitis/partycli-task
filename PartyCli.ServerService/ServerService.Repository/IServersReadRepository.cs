﻿using ServerService.DataContracts.Responses.Repositories;

namespace ServerService.Repository
{
    public interface IServersReadRepository
    {
        GetServersReadRepositoryResponse GetServersList();
    }
}