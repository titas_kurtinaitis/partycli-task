﻿using System.Linq;
using LiteDB;
using Microsoft.Extensions.Logging;
using ServerService.DataContracts.DomainModels;
using ServerService.DataContracts.Requests.Repositories;
using ServerService.DataContracts.Responses.Repositories;
using ServerService.Repository.Constants;

namespace ServerService.Repository
{
    public class ServersWriteRepository : IServersWriteRepository
    {
        private readonly LiteDatabase db;
        private readonly ILogger<ServersWriteRepository> logger;

        public ServersWriteRepository(ILogger<ServersWriteRepository> logger)
        {
            this.logger = logger;
            this.db = new LiteDatabase("../Servers.db");
        }

        public CreateServersRepositoryResponse CreateServersList(CreateServersListRepositoryRequest request)
        {
            this.logger.LogDebug("Creating servers list.");
            var col = this.db.GetCollection<Server>(RepositoryConstants.ServersCollectionName);

            return request.Servers
                .Select(server => col.Insert(server))
                .Any(bsonValue => bsonValue == null) ? null : new CreateServersRepositoryResponse();
        }
    }
}
