﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServerService.Commands;
using ServerService.DataContracts.Requests.Commands;
using ServerService.DataContracts.Responses.Commands;
using ServerService.Repository;
using SharedComponents.CommandsHandler;

namespace ServerService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var builder = new ContainerBuilder();

            // Register Commands
            builder.RegisterType<CreateServersListCommand>().As<ICommand<CreateServersListCommandRequest, CommandResponse<CreateServersListCommandResponse>>>();
            builder.RegisterType<GetServersListCommand>().As<ICommand<GetServersListCommandRequest, CommandResponse<GetServersListCommandResponse>>>();

            // Register Repositories
            builder.RegisterType<ServersReadRepository>().As<IServersReadRepository>().SingleInstance();
            builder.RegisterType<ServersWriteRepository>().As<IServersWriteRepository>().SingleInstance();


            //builder.RegisterType<ServerServiceRestClient>().As<IServerServiceRestClient>().SingleInstance();
            //builder.RegisterType<ServiceDiscoveryRestClient>().As<IServiceDiscoveryRestClient>().SingleInstance();


            //var sectionName = nameof(ServiceDiscoveryConfiguration);

            //var section = Configuration.GetSection(sectionName);

            //if (!section.Exists())
            //{
            //    throw new ArgumentNullException(sectionName);
            //}

            //services.Configure<ServiceDiscoveryConfiguration>(section);


            builder.Populate(services);
            this.ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
