﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServerService.DataContracts.Requests.Commands;
using ServerService.DataContracts.Responses.Commands;
using SharedComponents.CommandsHandler;
using SharedComponents.CommandsHandler.Enums;
using SharedComponents.ResponseHandler;
using ServerService.DataContracts.Requests;

namespace ServerService.Controllers
{
    [Route("servers")]
    public class ServersController : ApiController
    {
        private readonly ICommand<CreateServersListCommandRequest, CommandResponse<CreateServersListCommandResponse>> createServersListCommand;

        private readonly ICommand<GetServersListCommandRequest, CommandResponse<GetServersListCommandResponse>> getServersListCommand;

        public ServersController(ICommand<CreateServersListCommandRequest, CommandResponse<CreateServersListCommandResponse>> createServersListCommand, ICommand<GetServersListCommandRequest, CommandResponse<GetServersListCommandResponse>> getServersListCommand)
        {
            this.createServersListCommand = createServersListCommand;
            this.getServersListCommand = getServersListCommand;
        }

        [HttpPost]
        public IActionResult CreateServersList([FromBody] CreateServersListRequest createServersListRequest)
        {
            var commandRequest = new CreateServersListCommandRequest
            {
                Servers = createServersListRequest.Servers
            };

            var commandResponse = this.createServersListCommand.Execute(commandRequest);

            if (commandResponse.ResponseType != CommandResponseType.Success)
            {
                return this.CommandErrorResponse(commandResponse);
            }

            return this.Ok(commandResponse.Result);
        }

        [HttpGet]
        public IActionResult GetServersList()
        {
            var commandResponse = this.getServersListCommand.Execute(new GetServersListCommandRequest());

            if (commandResponse.ResponseType != CommandResponseType.Success)
            {
                return this.CommandErrorResponse(commandResponse);
            }

            return this.Ok(commandResponse.Result);
        }
    }
}
